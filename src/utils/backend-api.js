import axios from 'axios'

const BASE_URL = 'http://10.10.10.127:5000/'

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: false,
  params: {}
})

// Add a request interceptor
instance.interceptors.request.use(function (config) {
  config.headers.common['Authorization'] = 'Bearer '
  config.headers.common['Access-Control-Allow-Origin'] = '*'
  config.headers.common['Access-Control-Allow-Methods'] =  ('GET, POST, PATCH, PUT, DELETE, OPTIONS');
  config.headers.common['Access-Control-Allow-Headers'] =  ('Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

instance.interceptors.response.use((response) => response, (error) => {
  return Promise.reject(error)
})

export default {
  getData (action) {
    let url = `${BASE_URL}`
    url += action
    return instance.get(url)
  },
  postData (action, data) {
    let url = `${BASE_URL}`
    url += action
    return instance.post(url, data)
  },
  putData (action, data) {
    let url = `${BASE_URL}`
    url += action
    return instance.put(url, data)
  },
  deleteData (action) {
    let url = `${BASE_URL}`
    url += action
    return instance.delete(url)
  },
  login (action, data) {
    let url = `${BASE_URL}`
    url += action
    return instance.get(url, data)
  }
}
