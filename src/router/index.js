import Vue from 'vue'
import Router from 'vue-router'
import ErrorPage from '@/components/404'
import OrderList from '@/pages/OrderList'
import OrderForm from '@/pages/OrderForm'

Vue.use(Router)

// const debug = process.env.NODE_ENV !== 'production'

export default new Router({
  base: __dirname,
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { path: '/404', component: ErrorPage, name: 'ErrorPage' },
    { path: '/orders', component: OrderList, name: 'Orders' },
    { path: '/neworder', component: OrderForm, name: 'NewOrder' },
    { path: '*', redirect: '/404' }
  ],
  meta: {
    progress: {
      func: [
        {call: 'color', modifier: 'temp', argument: '#ffb000'},
        {call: 'fail', modifier: 'temp', argument: '#6e0000'},
        {call: 'location', modifier: 'temp', argument: 'top'},
        {call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 400}}
      ]
    }
  }
})
