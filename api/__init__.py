# -*- coding: utf-8 -*-

from flask import Flask, jsonify, redirect, url_for, render_template, request, session
from flask_cors import CORS
from flask_debug import Debug
import csv, redis, json
import json
import sys
import os

REDIS_HOST = 'localhost'
conn = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0, decode_responses=True)
conn.flushall()

app = Flask(__name__)
CORS(app)
app.secret_key = os.urandom(12)  

@app.route('/user', methods=['GET'])
def get_user():
    user_id = request.args.get('id')+".*"
    cards=[]
    for key in conn.scan_iter(user_id):
        card = json.loads(conn.lrange(key, 0, -1)[0])
        cards.append(card)
    
    return jsonify({"cards" : cards})

@app.route('/valid-thru', methods=['GET'])
def get_valid_date():
    card_date = "*."+request.args.get('date')
    cards=[]
    for key in conn.scan_iter(card_date):
        card = json.loads(conn.lrange(key, 0, -1)[0])
        cards.append(card)
    return jsonify({"cards" : cards})

@app.route('/card', methods=['POST'])
def store_new_card():
    print(request.data)
    card = json.loads(request.data)
    conn.lpush("".join([card.get("id"),".",card.get("date")]), json.dumps(card))
    return jsonify({"status" : "ok"})

def read_csv_data(csv_file):
    with open(csv_file, encoding='utf-8') as csvf:
        csv_data = csv.reader(csvf)
        next(csv_data)
        return [(r[0], r[1], r[2], r[3]) for r in csv_data]

def store_all_data(data):
    for i in data:
        if i[1] not in conn:
            card = {"id":i[0],"card":i[2],"name":i[1],"date":i[3]}
            conn.lpush("".join([i[0],".",i[3]]), json.dumps(card))
    return data

if __name__ == "__main__":
    data = read_csv_data("cards.csv")
    store_all_data(data)
    
    app.run(debug=True, use_reloader=True,host= '10.10.10.127')



