import unittest
import os
import json
import testing.redis
from redis import Redis
from time import sleep
from app import app

class BasicTestCase(unittest.TestCase):

    def test_is_empty(self):
        tester = app.test_client(self)
        response = tester.get('/user?id=1', content_type='html/text')
        cards=[]
        cards = json.loads(response.data).get("cards")
        print(cards)
        self.assertEqual(response.status_code, 200)

    def test_store(self):
        tester = app.test_client(self)
        response = tester.get('/user?id=1', content_type='html/text')
        cards=[]
        cards = json.loads(response.data).get("cards")
        self.assertEqual(len(cards), 0)
        response = tester.post('/card', json={'id':'1', 'card':'12345', 'name':'Test Name', 'date':'2012/01'})
        response = tester.get('/user?id=1', content_type='html/text')
        cards = json.loads(response.data).get("cards")
        self.assertEqual(len(cards), 1)

if __name__ == '__main__':
    unittest.main()